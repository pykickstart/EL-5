#
# Chris Lumens <clumens@redhat.com>
#
# Copyright 2005, 2006, 2007 Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  Any Red Hat
# trademarks that are incorporated in the source code or documentation are not
# subject to the GNU General Public License and may only be used or replicated
# with the express permission of Red Hat, Inc. 
#
import string

from imgcreate.pykickstart.base import *
from imgcreate.pykickstart.options import *

def append_const_cb( const ):
    def cb( option, opt_str, value, parser ):
        if not getattr(parser.values, option.dest):
            setattr(parser.values, option.dest, list())
        getattr(parser.values, option.dest).append(const)
    return cb
    

class FC3_Firewall(KickstartCommand):
    def __init__(self, writePriority=0, enabled=None, ports=None, trusts=None):
        KickstartCommand.__init__(self, writePriority)
        self.op = self._getParser()

        self.enabled = enabled

        if ports == None:
            ports = []

        self.ports = ports

        if trusts == None:
            trusts = []

        self.trusts = trusts

    def __str__(self):
        extra = []
        filteredPorts = []

        if self.enabled is None:
            return ""

        if self.enabled:
            # It's possible we have words in the ports list instead of
            # port:proto (s-c-kickstart may do this).  So, filter those
            # out into their own list leaving what we expect.
            for port in self.ports:
                if port == "ssh":
                    extra.append("--ssh")
                elif port == "telnet":
                    extra.append("--telnet")
                elif port == "smtp":
                    extra.append("--smtp")
                elif port == "http":
                    extra.append("--http")
                elif port == "ftp":
                    extra.append("--ftp")
                else:
                    filteredPorts.append(port)

            # All the port:proto strings go into a comma-separated list.
            portstr = string.join (filteredPorts, ",")
            if len(portstr) > 0:
                portstr = "--port=" + portstr
            else:
                portstr = ""

            extrastr = string.join (extra, " ")

            truststr = string.join (self.trusts, ",")
            if len(truststr) > 0:
                truststr = "--trust=" + truststr

            # The output port list consists only of port:proto for
            # everything that we don't recognize, and special options for
            # those that we do.
            return "# Firewall configuration\nfirewall --enabled %s %s %s\n" % (extrastr, portstr, truststr)
        else:
            return "# Firewall configuration\nfirewall --disabled\n"

    def _getParser(self):
        def firewall_port_cb (option, opt_str, value, parser):
            for p in value.split(","):
                p = p.strip()
                if p.find(":") == -1:
                    p = "%s:tcp" % p
                parser.values.ensure_value(option.dest, []).append(p)

        op = KSOptionParser(map={"ssh":["22:tcp"], "telnet":["23:tcp"],
                             "smtp":["25:tcp"], "http":["80:tcp", "443:tcp"],
                             "ftp":["21:tcp"]}, lineno=self.lineno)

        op.add_option("--disable", "--disabled", dest="enabled",
                      action="store_false")
        op.add_option("--enable", "--enabled", dest="enabled",
                      action="store_true", default=True)
        op.add_option("--ftp", "--http", "--smtp", "--ssh", "--telnet",
                      dest="ports", action="map_extend")
        op.add_option("--high", deprecated=1)
        op.add_option("--medium", deprecated=1)
        op.add_option("--port", dest="ports", action="callback",
                      callback=firewall_port_cb, nargs=1, type="string")
        op.add_option("--trust", dest="trusts", action="append")
        return op

    def parse(self, args):
        (opts, extra) = self.op.parse_args(args=args)
        self._setToSelf(self.op, opts)

class F9_Firewall(FC3_Firewall):
    def _getParser(self):
        op = FC3_Firewall._getParser(self)
        op.remove_option("--high")
        op.remove_option("--medium")
        return op

class F10_Firewall(F9_Firewall):
    def __init__(self, writePriority=0, enabled=None, ports=None, trusts=None,
                 services=None):
        F9_Firewall.__init__(self, writePriority, enabled=enabled, ports=ports,
                             trusts=trusts)

        if services == None:
            services = []

        self.services = services

    def __str__(self):
        if self.enabled is None:
            return ""

        retval = F9_Firewall.__str__(self)
        if self.enabled:
            retval = retval.strip()

            svcstr = string.join(self.services, ",")
            if len(svcstr) > 0:
                svcstr = " --service=" + svcstr
            else:
                svcstr = ""

            return retval + "%s\n" % svcstr
        else:
            return retval            

    def _getParser(self):
        def service_cb (option, opt_str, value, parser):
            for p in value.split(","):
                p = p.strip()
                parser.values.ensure_value(option.dest, []).append(p)

        op = F9_Firewall._getParser(self)
        op.add_option("--service", dest="services", action="callback",
                      callback=service_cb, nargs=1, type="string")
        op.add_option("--ftp", dest="services", action="callback", callback=append_const_cb("ftp"))
        op.add_option("--http", dest="services", action="callback", callback=append_const_cb("http"))
        op.add_option("--smtp", dest="services", action="callback", callback=append_const_cb("smtp"))
        op.add_option("--ssh", dest="services", action="callback", callback=append_const_cb("ssh"))
        op.add_option("--telnet", deprecated=1)
        return op
