#
# Chris Lumens <clumens@redhat.com>
#
# Copyright 2005, 2006, 2007 Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  Any Red Hat
# trademarks that are incorporated in the source code or documentation are not
# subject to the GNU General Public License and may only be used or replicated
# with the express permission of Red Hat, Inc. 
#
import string

from imgcreate.pykickstart.base import *
from imgcreate.pykickstart.constants import *
from imgcreate.pykickstart.errors import *
from imgcreate.pykickstart.options import *

class FC3_ClearPart(KickstartCommand):
    def __init__(self, writePriority=120, drives=None, initAll=False,
                 type=None):
        KickstartCommand.__init__(self, writePriority)
        self.op = self._getParser()

        if drives == None:
            drives = []

        self.drives = drives
        self.initAll = initAll
        self.type = type

    def __str__(self):
        if self.type is None:
            return ""

        if self.type == CLEARPART_TYPE_NONE:
            clearstr = "--none"
        elif self.type == CLEARPART_TYPE_LINUX:
            clearstr = "--linux"
        elif self.type == CLEARPART_TYPE_ALL:
            clearstr = "--all"
        else:
            clearstr = ""

        if self.initAll:
            initstr = "--initlabel"
        else:
            initstr = ""

        if len(self.drives) > 0:
            drivestr = "--drives=" + string.join (self.drives, ",")
        else:
            drivestr = ""

        return "# Partition clearing information\nclearpart %s %s %s\n" % (clearstr, initstr, drivestr)

    def _getParser(self):
        def drive_cb (option, opt_str, value, parser):
            for d in value.split(','):
                parser.values.ensure_value(option.dest, []).append(d)

        op = KSOptionParser(lineno=self.lineno)
        op.add_option("--all", dest="type", action="store_const",
                      const=CLEARPART_TYPE_ALL)
        op.add_option("--drives", dest="drives", action="callback",
                      callback=drive_cb, nargs=1, type="string")
        op.add_option("--initlabel", dest="initAll", action="store_true",
                      default=False)
        op.add_option("--linux", dest="type", action="store_const",
                      const=CLEARPART_TYPE_LINUX)
        op.add_option("--none", dest="type", action="store_const",
                      const=CLEARPART_TYPE_NONE)
        return op

    def parse(self, args):
        (opts, extra) = self.op.parse_args(args=args)
        self._setToSelf(self.op, opts)
