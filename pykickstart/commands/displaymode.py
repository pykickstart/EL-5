#
# Chris Lumens <clumens@redhat.com>
#
# Copyright 2005, 2006, 2007 Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  Any Red Hat
# trademarks that are incorporated in the source code or documentation are not
# subject to the GNU General Public License and may only be used or replicated
# with the express permission of Red Hat, Inc. 
#
from imgcreate.pykickstart.base import *
from imgcreate.pykickstart.constants import *
from imgcreate.pykickstart.options import *

class FC3_DisplayMode(KickstartCommand):
    def __init__(self, writePriority=0, displayMode=None):
        KickstartCommand.__init__(self, writePriority)
        self.displayMode = displayMode

    def __str__(self):
        if self.displayMode is None:
            return ""

        if self.displayMode == DISPLAY_MODE_CMDLINE:
            return "cmdline\n"
        elif self.displayMode == DISPLAY_MODE_GRAPHICAL:
            return "# Use graphical install\ngraphical\n"
        elif self.displayMode == DISPLAY_MODE_TEXT:
            return "# Use text mode install\ntext\n"

    def parse(self, args):
        if self.currentCmd == "cmdline":
            self.displayMode = DISPLAY_MODE_CMDLINE
        elif self.currentCmd == "graphical":
            self.displayMode = DISPLAY_MODE_GRAPHICAL
        elif self.currentCmd == "text":
            self.displayMode = DISPLAY_MODE_TEXT
