# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-10-04 13:22-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../pykickstart/base.py:147
#, python-format
msgid ""
"Ignoring deprecated command on line %(lineno)s:  The %(cmd)s command has "
"been deprecated and no longer has any effect.  It may be removed from future "
"releases, which will result in a fatal error from kickstart.  Please modify "
"your kickstart file to remove this command."
msgstr ""

#: ../pykickstart/base.py:315
#, python-format
msgid "Unknown command: %s"
msgstr ""

#: ../pykickstart/errors.py:48
#, python-format
msgid ""
"The following problem occurred on line %(lineno)s of the kickstart file:\n"
"\n"
"%(msg)s\n"
msgstr ""

#: ../pykickstart/errors.py:50
#, python-format
msgid "There was a problem reading from line %s of the kickstart file"
msgstr ""

#: ../pykickstart/options.py:84
#, python-format
msgid "Option %s is required"
msgstr ""

#: ../pykickstart/options.py:87
#, python-format
msgid ""
"The option %(option)s was introduced in version %(intro)s, but you are using "
"kickstart syntax version %(version)s"
msgstr ""

#: ../pykickstart/options.py:90
#, python-format
msgid ""
"Ignoring deprecated option on line %(lineno)s:  The %(option)s option has "
"been deprecated and no longer has any effect.  It may be removed from future "
"releases, which will result in a fatal error from kickstart.  Please modify "
"your kickstart file to remove this option."
msgstr ""

#: ../pykickstart/options.py:93
#, python-format
msgid ""
"The option %(option)s was removed in version %(removed)s, but you are using "
"kickstart syntax version %(version)s"
msgstr ""

#: ../pykickstart/options.py:147
msgid "Required flag set for option that doesn't take a value"
msgstr ""

#: ../pykickstart/options.py:156
#, python-format
msgid "Option %(opt)s: invalid boolean value: %(value)r"
msgstr ""

#: ../pykickstart/options.py:161
#, python-format
msgid "Option %(opt)s: invalid string value: %(value)r"
msgstr ""

#: ../pykickstart/parser.py:90
#, python-format
msgid "Illegal url for %%ksappend: %s"
msgstr ""

#: ../pykickstart/parser.py:95
#, python-format
msgid "Unable to open %%ksappend file: "
msgstr ""

#: ../pykickstart/parser.py:101 ../pykickstart/parser.py:103
#, python-format
msgid "Unable to open %%ksappend file"
msgstr ""

#: ../pykickstart/parser.py:286
msgid "Group cannot specify both --nodefaults and --optional"
msgstr ""

#: ../pykickstart/parser.py:545 ../pykickstart/parser.py:604
#, python-format
msgid ""
"%s does not end with %%end.  This syntax has been deprecated.  It may be "
"removed from future releases, which will result in a fatal error from "
"kickstart.  Please modify your kickstart file to use this updated syntax."
msgstr ""

#: ../pykickstart/parser.py:604
msgid "Script"
msgstr ""

#. If nothing else worked, we're out of options.
#: ../pykickstart/version.py:87 ../pykickstart/version.py:96
#: ../pykickstart/version.py:99 ../pykickstart/version.py:110
#: ../pykickstart/version.py:150
#, python-format
msgid "Unsupported version specified: %s"
msgstr ""

#: ../tools/ksvalidator:52
msgid "halt after the first error or warning"
msgstr ""

#: ../tools/ksvalidator:55
#, python-format
msgid "parse include files when %include is seen"
msgstr ""

#: ../tools/ksvalidator:57
msgid "version of kickstart syntax to validate against"
msgstr ""

#: ../tools/ksvalidator:71
#, python-format
msgid "The version %s is not supported by pykickstart"
msgstr ""

#: ../tools/ksvalidator:87
#, python-format
msgid ""
"File uses a deprecated option or command.\n"
"%s"
msgstr ""

#: ../tools/ksvalidator:93
msgid "General kickstart error in input file"
msgstr ""

#: ../tools/ksvalidator:96
#, python-format
msgid "General error in input file:  %s"
msgstr ""
