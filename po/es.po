# translation of es.po to Spanish
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Manuel Ospina <mospina@redhat.com>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: pykickstart\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-10-04 13:22-0400\n"
"PO-Revision-Date: 2007-10-05 10:24-0300\n"
"Last-Translator: Domingo Becker <domingobecker@gmail.com>\n"
"Language-Team: Fedora Spanish <fedora-trans-es@redhat.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"
"X-Poedit-Language: Spanish\n"

#: ../pykickstart/base.py:147
#, python-format
msgid "Ignoring deprecated command on line %(lineno)s:  The %(cmd)s command has been deprecated and no longer has any effect.  It may be removed from future releases, which will result in a fatal error from kickstart.  Please modify your kickstart file to remove this command."
msgstr "Se ignora comando descartado en línea %(lineno)s: El comando %(cmd)s fue descartado y ya no produce efecto. Puede ser eliminado de versiones futuras, lo que resultará en un error fatal desde el archivo de respuestas (kickstart). Por favor, modifique su archivo de respuestas (kickstart) y elimine este comando."

#: ../pykickstart/base.py:315
#, python-format
msgid "Unknown command: %s"
msgstr "Comando desconocido: %s"

#: ../pykickstart/errors.py:48
#, python-format
msgid ""
"The following problem occurred on line %(lineno)s of the kickstart file:\n"
"\n"
"%(msg)s\n"
msgstr ""
"El siguiente problema ocurrió en la línea %(lineno)s del archivo de respuestas (kickstart):\n"
"\n"
"%(msg)s\n"

#: ../pykickstart/errors.py:50
#, python-format
msgid "There was a problem reading from line %s of the kickstart file"
msgstr "Hubo un problema al leer la línea %s del archivo de respuestas (kickstart)"

#: ../pykickstart/options.py:84
#, python-format
msgid "Option %s is required"
msgstr "Se requiere la opción %s"

#: ../pykickstart/options.py:87
#, python-format
msgid "The option %(option)s was introduced in version %(intro)s, but you are using kickstart syntax version %(version)s"
msgstr "La opción %(option)s fue introducida en la versión %(intro)s, y usted está usando la sintaxis del kickstart versión %(version)s"

#: ../pykickstart/options.py:90
#, python-format
msgid "Ignoring deprecated option on line %(lineno)s:  The %(option)s option has been deprecated and no longer has any effect.  It may be removed from future releases, which will result in a fatal error from kickstart.  Please modify your kickstart file to remove this option."
msgstr "Se ignora opción obsoleta en línea %(lineno)s: la opción %(option)s ha sido descartada y ya no tiene efecto. Puede ser borrada de versiones futuras, lo que resultará en un error fatal desde el archivo de respuestas (kickstart). Por favor, modifique su archivo de respuestas (kickstart) y elimine esta opción."

#: ../pykickstart/options.py:93
#, python-format
msgid "The option %(option)s was removed in version %(removed)s, but you are using kickstart syntax version %(version)s"
msgstr "La opción %(option)s fue eliminada en la versión %(removed)s, y usted está usando la sintaxis del kickstart versión %(version)s"

#: ../pykickstart/options.py:147
msgid "Required flag set for option that doesn't take a value"
msgstr "Se requiere poner una bandera para una opción que no toma un valor"

#: ../pykickstart/options.py:156
#, python-format
msgid "Option %(opt)s: invalid boolean value: %(value)r"
msgstr "Opción %(opt)s: valor booleano inválido: %(value)r"

#: ../pykickstart/options.py:161
#, python-format
msgid "Option %(opt)s: invalid string value: %(value)r"
msgstr "Opción %(opt)s: valor cadena inválido: %(value)r"

#: ../pykickstart/parser.py:90
#, python-format
msgid "Illegal url for %%ksappend: %s"
msgstr "URL ilegal para %%ksappend: %s"

#: ../pykickstart/parser.py:95
#, python-format
msgid "Unable to open %%ksappend file: "
msgstr "No se pudo abrir el archivo %%ksappend:"

#: ../pykickstart/parser.py:101
#: ../pykickstart/parser.py:103
#, python-format
msgid "Unable to open %%ksappend file"
msgstr "No se pudo abrir el archivo %%ksappend"

#: ../pykickstart/parser.py:286
msgid "Group cannot specify both --nodefaults and --optional"
msgstr "El grupo no puede especificar --nodefaults y --optional a la vez."

#: ../pykickstart/parser.py:545
#: ../pykickstart/parser.py:604
#, python-format
msgid "%s does not end with %%end.  This syntax has been deprecated.  It may be removed from future releases, which will result in a fatal error from kickstart.  Please modify your kickstart file to use this updated syntax."
msgstr "%s no termina con %%end. Esta sintaxis ya no es usada. Puede ser eliminada de versiones futuras, lo que puede resultar en un error fatal desde el archivo de respuestas kickstart. Por favor, modifique su archivo de respuestas kickstart para que se use la sintaxis actualizada."

#: ../pykickstart/parser.py:604
msgid "Script"
msgstr "Script"

#. If nothing else worked, we're out of options.
#: ../pykickstart/version.py:87
#: ../pykickstart/version.py:96
#: ../pykickstart/version.py:99
#: ../pykickstart/version.py:110
#: ../pykickstart/version.py:150
#, python-format
msgid "Unsupported version specified: %s"
msgstr "Se especificó una versión no soportada: %s"

#: ../tools/ksvalidator:52
msgid "halt after the first error or warning"
msgstr "parar después del primer error o advertencia"

#: ../tools/ksvalidator:55
#, python-format
msgid "parse include files when %include is seen"
msgstr "examinar archivos include cuando se vea %include"

#: ../tools/ksvalidator:57
msgid "version of kickstart syntax to validate against"
msgstr "versión de la sintaxis de kickstart con la cual validar"

#: ../tools/ksvalidator:71
#, python-format
msgid "The version %s is not supported by pykickstart"
msgstr "La versión %s no es soportada por pykickstart"

#: ../tools/ksvalidator:87
#, python-format
msgid ""
"File uses a deprecated option or command.\n"
"%s"
msgstr ""
"El archivo está usando una opción o comando obsoleto.\n"
"%s"

#: ../tools/ksvalidator:93
msgid "General kickstart error in input file"
msgstr "Error general en el archivo de respuestas (kickstart) de entrada"

#: ../tools/ksvalidator:96
#, python-format
msgid "General error in input file:  %s"
msgstr "Error general en el archivo de entrada: %s"

